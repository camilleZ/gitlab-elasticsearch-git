require 'spec_helper'

describe TestRepository do
  before do
    remove_index(TestRepository.index_name)
    TestRepository.__elasticsearch__.create_index!
  end

  let(:repo) { TestRepository.new }

  it "creates an index" do
    expect(index_exist?(TestRepository.index_name)).to be_truthy
  end

  it "indexes all blobs and searches" do
    repo.index_blobs

    TestRepository.__elasticsearch__.refresh_index!

    expect(repo.search('def', type: :blob)[:blobs][:total_count]).to eq(4)
  end

  it "indexes all commits and searches" do
    repo.index_commits

    TestRepository.__elasticsearch__.refresh_index!

    expect(repo.search('test', type: :commit)[:commits][:total_count]).to eq(3)
  end

  it "searches through all types" do
    repo.index_commits
    repo.index_blobs

    TestRepository.__elasticsearch__.refresh_index!

    expect(repo.search('test')[:commits][:total_count]).to eq(3)
    expect(repo.search('def')[:blobs][:total_count]).to eq(4)
  end

  it "searches through filename" do
    repo.index_blobs

    TestRepository.__elasticsearch__.refresh_index!

    found_version_file = repo.search('version')[:blobs][:results].any? do |result|
      result["_source"]["blob"]["file_name"] == "VERSION"
    end

    expect(found_version_file).to be_truthy
  end

  it "searches through camel cased words" do
    TestRepository.__elasticsearch__.create_index!(force: true)
    repo.index_blobs

    TestRepository.__elasticsearch__.refresh_index!

    found_version_file = repo.search('Hip')[:blobs][:results].any? do |result|
      result["_source"]["blob"]["file_name"] == "camelCase.rb"
    end

    expect(found_version_file).to be_truthy
  end

  it "supports advanced search syntax" do
    repo.index_blobs
    TestRepository.__elasticsearch__.refresh_index!

    expect(repo.search('def +(popen | username_regex)')[:blobs][:total_count]).to eq(2)
  end

  it "indexes specified commits" do
    repo.index_commits(
      from_rev: '40f4a7a617393735a95a0bb67b08385bc1e7c66d',
      to_rev: '732401c65e924df81435deb12891ef570167d2e2'
    )

    TestRepository.__elasticsearch__.refresh_index!

    expect(repo.search('empty', type: :commit)[:commits][:total_count]).to eq(1)
  end

  it "indexes specified blobs" do
    repo.index_blobs(
      from_rev: '40f4a7a617393735a95a0bb67b08385bc1e7c66d',
      to_rev: '732401c65e924df81435deb12891ef570167d2e2'
    )

    TestRepository.__elasticsearch__.refresh_index!

    expect(repo.search('Permission is hereby granted', type: :blob)[:blobs][:total_count]).to eq(1)
  end

  it "applies repository_id filter for blobs" do
    repo.index_blobs

    TestRepository.new("repo_second").index_blobs

    TestRepository.__elasticsearch__.refresh_index!

    expect(TestRepository.__elasticsearch__.search('def').results.count).to eq(8)
    expect(repo.search('def')[:blobs][:total_count]).to eq(4)
  end

  it "applies repository_id filter for commits" do
    repo.index_commits

    TestRepository.new("repo_second").index_commits

    TestRepository.__elasticsearch__.refresh_index!

    expect(TestRepository.__elasticsearch__.search('test').results.count).to eq(6)
    expect(repo.search('test')[:commits][:total_count]).to eq(3)
  end

  it "yields current batch and total length when indexing commits" do
    count = 0
    total = 0

    repo.index_commits do |batch, total_count|
      count += batch.length
      total  = total_count
    end

    expect(count).to be > 0
    expect(count).to eq(total)
  end

  it "yields current batch and total length when indexing blobs" do
    count = 0
    total = 0


    repo.index_blobs do |batch, total_count|
      count += batch.length
      total  = total_count
    end

    expect(count).to be > 0
    expect(count).to eq(total)
  end
end
