module ElasticHelper
  def index_exist?(index_name)
    out, err, status = Open3.capture3("curl -XGET '#{elastic_url}/#{index_name}/'")
    !out.include?('index_not_found_exception')
  end

  def remove_index(index_name)
    system("curl -XDELETE '#{elastic_url}#{index_name}/' > /dev/null 2>&1")
  end

  def elastic_url
    host = ENV['ELASTICSEARCH_HOST'] || 'localhost'
    "http://#{host}:9200/"
  end
end
