require 'pry'
require 'elasticsearch/git'

require_relative 'support/seed_helper'
require_relative 'support/elastic_helper'
require_relative 'support/test_repository'

SUPPORT_PATH = File.join(File.expand_path(File.dirname(__FILE__)), '../support')
TEST_REPO_PATH = File.join(SUPPORT_PATH, 'gitlab-elasticsearch-git-test.git')

RSpec.configure do |config|
  config.order = 'random'
  config.include ElasticHelper
  config.include SeedHelper
  config.before(:all) { ensure_seeds }
end
